package com.Folcademy.banco_quebrado.exceptions;

public class NotAuthoritionException extends RuntimeException {
    public NotAuthoritionException(String message) {
        super(message);
    }
}
