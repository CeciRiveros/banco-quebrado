package com.Folcademy.banco_quebrado.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultErrorHandler(HttpServletRequest request, Exception e){
        e.printStackTrace();
        return new ResponseEntity<>(new ErrorMessage("Error Generico",
                                    e.getMessage(),"1",
                                    request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> notFoundErrorHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Error not Found",
                                    e.getMessage(),"2",
                                    request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NotAuthoritionException.class, UsernameNotFoundException.class, BadCredentialsException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> notAuthoritionExceptionHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Error Not Authorition",
                                    e.getMessage(),"4",
                                    request.getRequestURI()), HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler({BadRequestException.class, HttpMessageNotReadableException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> badRequestException(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Error Bad Request",
                e.getMessage(),"3",
                request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }

}
