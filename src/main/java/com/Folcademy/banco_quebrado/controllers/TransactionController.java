package com.Folcademy.banco_quebrado.controllers;
import com.Folcademy.banco_quebrado.models.dto.NewTransactionDTO;
import com.Folcademy.banco_quebrado.models.dto.OnlyTransactionDTO;
import com.Folcademy.banco_quebrado.models.dto.TransactionDTO;
import com.Folcademy.banco_quebrado.models.dto.TransactionsDTO;
import com.Folcademy.banco_quebrado.service.TransactionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private  final TransactionsService transactionsService;

    public TransactionController( TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }


    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTransactionForAccounts(@PathVariable Long accountNumber){
        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }
    @GetMapping("/{transactionId}")
    public ResponseEntity<OnlyTransactionDTO> getTransactionForId(@PathVariable Long transactionId){
        return new ResponseEntity<>(transactionsService.getOnlyTransaction(transactionId), HttpStatus.OK);

    }
    @PostMapping("/new_transactions")
    public  ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO){
        return transactionsService.createTransaction(newTransactionDTO);
    }



}
