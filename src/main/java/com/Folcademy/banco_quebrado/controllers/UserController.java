package com.Folcademy.banco_quebrado.controllers;

import com.Folcademy.banco_quebrado.models.dto.UserDTO;
import com.Folcademy.banco_quebrado.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{cuil}")
    public ResponseEntity<UserDTO> getUserByCuil(@PathVariable String cuil){
        return new ResponseEntity<>( userService.getUser(cuil),HttpStatus.OK);
    }

    @PostMapping("/new_user")
    public  ResponseEntity<String>  newUser(@RequestBody UserDTO userDTO){
        return userService.newUser(userDTO);
    }

    @DeleteMapping("/{cuil}")
    public ResponseEntity<String> deleteUser(@PathVariable String cuil){

        return userService.deleteUser(cuil);
    }

}
