package com.Folcademy.banco_quebrado.controllers;

import com.Folcademy.banco_quebrado.exceptions.NotAuthoritionException;
import com.Folcademy.banco_quebrado.models.dto.AuthenticateRequest;
import com.Folcademy.banco_quebrado.models.dto.AuthenticationResponse;
import com.Folcademy.banco_quebrado.segurity.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.net.Authenticator;
import java.util.Objects;

@RestController
@RequestMapping("/login")
public class AuthenticateController {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> createToken(@RequestBody AuthenticateRequest authenticateRequest){
                UserDetails userDetails;

                try{
                    userDetails= userDetailsService.loadUserByUsername(authenticateRequest.getUsername());
                }catch (UsernameNotFoundException e){
                    throw new NotAuthoritionException("Username or password incorrect");
                }
                if(Objects.isNull(userDetails) || !userDetails.getPassword().equals(authenticateRequest.getPassword())){
                    throw new NotAuthoritionException("El usuario o la contraseña no son correctos.");
                }

                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(),authenticateRequest.getPassword())
                );

                String jwt="Bearer" + jwtUtil.generateToken(userDetails);
                AuthenticationResponse response= new AuthenticationResponse(jwt);

        return ResponseEntity.ok(response);
    }
}
