package com.Folcademy.banco_quebrado.controllers;

import com.Folcademy.banco_quebrado.models.dto.AccountFullDTO;
import com.Folcademy.banco_quebrado.service.AccountsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {

    private  final AccountsService accountsService;

    public AccountsController(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @GetMapping("/{cbu}")
    public ResponseEntity<AccountFullDTO> getCBUAccounts(@PathVariable String cbu){
     return new ResponseEntity<> (accountsService.getAccount(cbu),HttpStatus.OK);
    }

    @PostMapping("/new")
    public  ResponseEntity<String>  newAccount(@RequestBody AccountFullDTO accountFullDTO){
        return accountsService.newAccount(accountFullDTO);
    }

}
