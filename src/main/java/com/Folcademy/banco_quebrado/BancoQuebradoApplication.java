package com.Folcademy.banco_quebrado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoQuebradoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoQuebradoApplication.class, args);
	}

}
