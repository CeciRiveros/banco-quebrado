package com.Folcademy.banco_quebrado.models.repository;

import com.Folcademy.banco_quebrado.models.entity.AccountsEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<AccountsEntity,Long> {
    List<AccountsEntity> findAllByUserId(String userId);
    AccountsEntity findByCbu(String cbu);

}
