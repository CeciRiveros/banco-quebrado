package com.Folcademy.banco_quebrado.models.repository;

import com.Folcademy.banco_quebrado.models.entity.TransactionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionsRepository extends CrudRepository<TransactionEntity,Long> {
    List<TransactionEntity> findAllByOrigin(String Origin);
    List<TransactionEntity> findAllByOriginOrDestination(String account, String account1);
}
