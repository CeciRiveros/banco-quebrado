package com.Folcademy.banco_quebrado.models.repository;

import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<UserEntity,String> {
    UserEntity findByCuil(String cuil);
    Optional<UserEntity> findByUsername(String username);

}
