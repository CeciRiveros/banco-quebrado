package com.Folcademy.banco_quebrado.models.mappers;

import com.Folcademy.banco_quebrado.models.dto.NewTransactionDTO;
import com.Folcademy.banco_quebrado.models.entity.TransactionEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionMappers {
    public TransactionEntity mapTransactionDtoToTransactionEntity(NewTransactionDTO newTransactionDTO){
        return new TransactionEntity(new Date(),
                newTransactionDTO.getDescription(),
                newTransactionDTO.getAmount(),
                newTransactionDTO.getCurrency(),
                newTransactionDTO.getFrom(),
                newTransactionDTO.getTo()
        );
    }
}
