package com.Folcademy.banco_quebrado.models.mappers;

import com.Folcademy.banco_quebrado.models.dto.UserDTO;
import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMappers {

    public UserEntity mapUserToUserEntity(UserDTO userDTO){

        UserEntity userEntity= new UserEntity(userDTO.getCuil(),
                                    userDTO.getFirstName(),
                                    userDTO.getLastName(),
                                    userDTO.getAddress());
        return userEntity;
    }
}
