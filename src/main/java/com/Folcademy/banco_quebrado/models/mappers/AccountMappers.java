package com.Folcademy.banco_quebrado.models.mappers;

import com.Folcademy.banco_quebrado.models.dto.AccountFullDTO;
import com.Folcademy.banco_quebrado.models.entity.AccountsEntity;
import org.springframework.stereotype.Component;
import com.Folcademy.banco_quebrado.models.enums.AccountType;

@Component
public class AccountMappers {
    public AccountsEntity mapAccountToAccountEntity(AccountFullDTO accountFullDTO){
        AccountsEntity accountsEntity = new AccountsEntity(
                accountFullDTO.getAccountNumber(),
                accountFullDTO.getCbu(),
                (AccountType) accountFullDTO.getAccountType(),
                accountFullDTO.getUserId()
        );
        return accountsEntity;
    }
}
