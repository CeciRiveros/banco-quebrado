package com.Folcademy.banco_quebrado.models.dto;

import java.math.BigDecimal;

public class TransactionDTO {

    private String date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String from;
    private String to;
    private  String type;

    public TransactionDTO() {
    }

    public TransactionDTO(String date, String description, BigDecimal amount, String currency, String origin, String destination, String type) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = origin;
        this.to = destination;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
