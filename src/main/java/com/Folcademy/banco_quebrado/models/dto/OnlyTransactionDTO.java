package com.Folcademy.banco_quebrado.models.dto;

import java.math.BigDecimal;

public class OnlyTransactionDTO {
    private String description;
    private BigDecimal amount;
    private String currency;
    private AccountDTO from;
    private AccountDTO to;
    private String date;

    public OnlyTransactionDTO(String description, BigDecimal amount, String currency, AccountDTO from, AccountDTO to, String date) {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountDTO getFrom() {
        return from;
    }

    public AccountDTO getTo() {
        return to;
    }

    public String getDate() {
        return date;
    }
}
