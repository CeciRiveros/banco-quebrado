package com.Folcademy.banco_quebrado.models.dto;

public class UserDTO {
    private String firstName;
    private  String LastName;
    private String cuil;
    private  String address;

    public UserDTO(String firstName, String lastName, String cuil, String address) {
        this.firstName = firstName;
        this.LastName = lastName;
        this.cuil = cuil;
        this.address = address;
    }

    public UserDTO() {

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getCuil() {
        return cuil;
    }

    public String getAddress() {
        return address;
    }
}
