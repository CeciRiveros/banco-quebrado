package com.Folcademy.banco_quebrado.models.dto;

public class AccountDTO {
    private String firstName;
    private String lastName;
    private String cbu;


    public AccountDTO(String firstName, String lastName, String cbu) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cbu = cbu;
    }

    public AccountDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCbu() {
        return cbu;
    }
}
