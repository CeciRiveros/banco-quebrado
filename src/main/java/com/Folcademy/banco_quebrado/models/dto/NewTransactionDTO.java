package com.Folcademy.banco_quebrado.models.dto;

import java.math.BigDecimal;
import java.util.Currency;

public class NewTransactionDTO {
    private String description;
    private BigDecimal amount;
    private String currency;
    private String from;
    private String to;

    public NewTransactionDTO( String description, BigDecimal amount, String currency, String from, String to) {

        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
    }

    public NewTransactionDTO() {
    }



    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
