package com.Folcademy.banco_quebrado.models.dto;

import com.Folcademy.banco_quebrado.models.enums.AccountType;

public class AccountFullDTO {
    private String firstName;
    private String lastName;
    private Long accountNumber;
    private String cbu;
    private AccountType accountType;
    public  String userId;

    public AccountFullDTO(String firstName, String lastName,Long accountNumber, String cbu, AccountType accountType, String userId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNumber = accountNumber;
        this.cbu = cbu;
        this.accountType = accountType;
        this.userId= userId;
    }

    public AccountFullDTO(Long accountNumber, String cbu, AccountType accountType, String userId) {
        this.accountNumber = accountNumber;
        this.cbu = cbu;
        this.accountType = accountType;
        this.userId = userId;
    }

    public AccountFullDTO() {

    }

    public String getUserId() {
        return userId;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountType getAccountType() {
        return accountType;
    }
}
