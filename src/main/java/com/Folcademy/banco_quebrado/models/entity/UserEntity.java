package com.Folcademy.banco_quebrado.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {
    @Id
    private String cuil;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String address;
    private String username;
    private String password;

    public UserEntity(String cuil, String firstName, String lastName, String address, String username, String password) {
        this.cuil = cuil;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.username = username;
        this.password = password;
    }

    public UserEntity(String cuil, String firstName, String lastName, String address) {
        this.cuil = cuil;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public UserEntity() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCuil() {
        return cuil;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getAddress() {
        return address;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

