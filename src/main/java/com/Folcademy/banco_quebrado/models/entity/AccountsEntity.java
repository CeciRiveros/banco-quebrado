package com.Folcademy.banco_quebrado.models.entity;

import com.Folcademy.banco_quebrado.models.enums.AccountType;

import javax.persistence.*;

@Entity(name = "accounts")
public class AccountsEntity {
    @Id
    private Long number;

    private String cbu;

    @Enumerated(EnumType.STRING)
    private AccountType type;

    private String userId;

    public AccountsEntity(Long number, String cbu, AccountType type, String userId) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userId = userId;
    }

    public AccountsEntity() {
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
