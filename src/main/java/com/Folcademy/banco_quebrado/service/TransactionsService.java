package com.Folcademy.banco_quebrado.service;

import com.Folcademy.banco_quebrado.exceptions.BadRequestException;
import com.Folcademy.banco_quebrado.models.dto.*;
import com.Folcademy.banco_quebrado.models.entity.AccountsEntity;
import com.Folcademy.banco_quebrado.models.entity.TransactionEntity;
import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import com.Folcademy.banco_quebrado.models.mappers.TransactionMappers;
import com.Folcademy.banco_quebrado.models.repository.AccountRepository;
import com.Folcademy.banco_quebrado.models.repository.TransactionsRepository;
import com.Folcademy.banco_quebrado.models.repository.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class TransactionsService {
    private  final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private  final UsersRepository  usersRepository;
    private  final TransactionMappers transactionMappers;


    public TransactionsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UsersRepository usersRepository, TransactionMappers transactionMappers) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.transactionMappers = transactionMappers;
    }

    public TransactionsDTO getTransactions(Long number){
      List<TransactionEntity> transactionEntities = transactionsRepository.findAllByOriginOrDestination(number.toString(),number.toString());
      List<TransactionDTO> transactionDTOList= new ArrayList<>();

      for(TransactionEntity entity: transactionEntities){
          AccountsEntity toAccountsEntity=accountRepository.findByCbu(entity.getDestination());
          AccountsEntity fromAccountsEntity=accountRepository.findByCbu(entity.getOrigin());
          UserEntity toUserEntity= usersRepository.findByCuil(toAccountsEntity.getUserId());

          String transactionType;
          if (entity.getOrigin().equals(number.toString())) {
              transactionType= "GASTO";
          }else{
              transactionType= "INGRESO";
          }

          transactionDTOList.add(
            new TransactionDTO(
                    entity.getDate().toString(),
                    entity.getDescription(),
                    entity.getAmount(),
                    entity.getCurrency(),
                    entity.getOrigin(),
                    entity.getDestination(),
                    transactionType
          ));

      }
      BigDecimal balance=BigDecimal.ZERO;
        for(TransactionDTO transactionDTO: transactionDTOList){
            if(transactionDTO.getType().equals("GASTO")){
                balance= balance.subtract(transactionDTO.getAmount());
            }else{
                balance= balance.add(transactionDTO.getAmount());
            }
        }
      return new TransactionsDTO(transactionDTOList, balance);
    }

    public OnlyTransactionDTO getOnlyTransaction(Long id){
        Optional<TransactionEntity> transactionEntityOnlyOptional= transactionsRepository.findById(id);
        if(transactionEntityOnlyOptional.isEmpty()){
            return null;
        }
        TransactionEntity transactionEntityOnly= transactionEntityOnlyOptional.get();
        AccountsEntity toAccountsEntity1=accountRepository.findByCbu(transactionEntityOnly.getOrigin());
        AccountsEntity toAccountsEntity2=accountRepository.findByCbu(transactionEntityOnly.getDestination());

        UserEntity toUserEntity1= usersRepository.findByCuil(toAccountsEntity1.getUserId());
        UserEntity toUserEntity2= usersRepository.findByCuil(toAccountsEntity2.getUserId());

        AccountDTO FromDTO= new AccountDTO(toUserEntity1.getFirstName(),toUserEntity1.getLastName(),transactionEntityOnly.getOrigin());
        AccountDTO ToDTO= new AccountDTO(toUserEntity2.getFirstName(),toUserEntity2.getLastName(),transactionEntityOnly.getDestination());

        OnlyTransactionDTO onlyTransactionDTO = new OnlyTransactionDTO(
                transactionEntityOnly.getDescription(),
                transactionEntityOnly.getAmount(),
                transactionEntityOnly.getCurrency(),
                FromDTO,
                ToDTO,
                transactionEntityOnly.getDate().toString());
        return onlyTransactionDTO;
    }

    public ResponseEntity<String> createTransaction(NewTransactionDTO newTransactionDTO){

            TransactionEntity transactionEntity= transactionMappers.mapTransactionDtoToTransactionEntity(newTransactionDTO);
            if(transactionEntity.getOrigin().equals(transactionEntity.getDestination())){
                throw new BadRequestException("Error al crear la transacción. From y to no pueden ser iguales.");
            }
            if(transactionEntity.getAmount().doubleValue()<=0){
                throw new BadRequestException("Error al crear la transacción. Amount debe ser mayor que 0.");
            }

            if(accountRepository.findByCbu(transactionEntity.getDestination()).getCbu().isEmpty()){
               System.out.println("vacia cbu");
                throw new BadRequestException("Error al crear la transacción, el cbu de destino no existe.");
            }

           transactionsRepository.save(transactionEntity);
           return new ResponseEntity<>("transacción creada", HttpStatus.OK);

    }



}
