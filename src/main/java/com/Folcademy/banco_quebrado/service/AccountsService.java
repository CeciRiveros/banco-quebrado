package com.Folcademy.banco_quebrado.service;

import com.Folcademy.banco_quebrado.exceptions.BadRequestException;
import com.Folcademy.banco_quebrado.exceptions.NotFoundException;
import com.Folcademy.banco_quebrado.models.dto.AccountFullDTO;
import com.Folcademy.banco_quebrado.models.entity.AccountsEntity;
import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import com.Folcademy.banco_quebrado.models.mappers.AccountMappers;
import com.Folcademy.banco_quebrado.models.repository.AccountRepository;
import com.Folcademy.banco_quebrado.models.repository.TransactionsRepository;
import com.Folcademy.banco_quebrado.models.repository.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {
    private  final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private  final UsersRepository usersRepository;
    private final AccountMappers accountMappers;

    public AccountsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UsersRepository usersRepository, AccountMappers accountMappers) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.accountMappers = accountMappers;
    }

    public AccountFullDTO getAccount(String cbu){
        AccountsEntity accountEntity= accountRepository.findByCbu(cbu);
        UserEntity userEntity= usersRepository.findByCuil(accountEntity.getUserId());
        AccountFullDTO accountFullDTO= new AccountFullDTO(userEntity.getFirstName(),
                                                        userEntity.getLastName(),
                                                        accountEntity.getNumber(),
                                                        accountEntity.getCbu(),
                                                        accountEntity.getType(),
                                                        accountEntity.getUserId());
        return accountFullDTO;
    }

    public ResponseEntity<String>  newAccount(AccountFullDTO accountFullDTO){
        AccountsEntity accountEntity= accountMappers.mapAccountToAccountEntity(accountFullDTO);

        if(accountRepository.findById(accountEntity.getNumber()).isEmpty()){
            throw new BadRequestException("No se puede crear cuenta, el número de cuenta ya exite.");
        }
        if(usersRepository.findById(accountEntity.getUserId()).isEmpty()){
            throw new BadRequestException("No se puede crear cuenta, ya que el usuario con el que se quiere asociar esa cuenta no existe");
        }

        accountRepository.save(accountEntity);
        return new ResponseEntity<>("Cuenta creada", HttpStatus.OK);
    }
}
