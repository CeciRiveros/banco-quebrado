package com.Folcademy.banco_quebrado.service;

import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import com.Folcademy.banco_quebrado.models.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class jpaUserDetailsService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Optional<UserEntity> userEntityOptional=usersRepository.findByUsername(username);
       if(userEntityOptional.isEmpty()){
           throw new UsernameNotFoundException("Not found:" + username);
       }
       UserEntity userEntity= userEntityOptional.get();
        return new User(userEntity.getUsername(),userEntity.getPassword(),new ArrayList<>());
    }
}
