package com.Folcademy.banco_quebrado.service;

import com.Folcademy.banco_quebrado.exceptions.BadRequestException;
import com.Folcademy.banco_quebrado.exceptions.NotFoundException;
import com.Folcademy.banco_quebrado.models.dto.AccountFullDTO;
import com.Folcademy.banco_quebrado.models.dto.UserDTO;
import com.Folcademy.banco_quebrado.models.entity.AccountsEntity;
import com.Folcademy.banco_quebrado.models.entity.UserEntity;
import com.Folcademy.banco_quebrado.models.mappers.UserMappers;
import com.Folcademy.banco_quebrado.models.repository.AccountRepository;
import com.Folcademy.banco_quebrado.models.repository.TransactionsRepository;
import com.Folcademy.banco_quebrado.models.repository.UsersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private  final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private  final UsersRepository usersRepository;
    private final UserMappers userMappers;

    public UserService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UsersRepository usersRepository, UserMappers userMappers) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.userMappers = userMappers;
    }
    public UserDTO getUser(String cuil){
        UserEntity userEntity= usersRepository.findByCuil(cuil);

        UserDTO userDTO= new UserDTO(
                userEntity.getFirstName(),
                userEntity.getLastName(),
                userEntity.getCuil(),
                userEntity.getAddress());

        return userDTO;
    }


    public ResponseEntity<String> newUser(UserDTO userDTO){
        UserEntity userEntity= userMappers.mapUserToUserEntity(userDTO);

        if(userDTO.getCuil().length()!=11){
          throw new BadRequestException("No se puede crear usuario, el cuil debe tener 11 digitos");
        }
       usersRepository.save(userEntity);
        return new ResponseEntity<>("Usuario creado", HttpStatus.OK);
    }

    public  ResponseEntity<String> deleteUser(String cuil){
        if(usersRepository.findById(cuil).isEmpty()){
            throw new NotFoundException("No se encontro usuario con ese cuil para eliminar");
        }
        usersRepository.deleteById(cuil);
        return new ResponseEntity<>("Usuario Eliminado correctamente", HttpStatus.OK);
    }
}
